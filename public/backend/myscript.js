function locdau(str, fill_to) {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|”|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'||\"|\&|\#|\[|\]|~|$|_/g, "");
    /* tìm và thay thế các kí tự đặc biệt trong chuỗi sang kí tự - */
    str = str.trim();
    str = str.replace(/\s+/g, "-");
    str = str.replace(/-+-/g, "-");//thay thế 2- thành 1-
    str = str.replace(/^\-+|\-+$/g, "-");//cắt bỏ ký tự - ở đầu và cuối chuỗi
    str = str.replace (/[^A-Za-z0-9\-]/g,'' );
    str = $.trim(str, "-");
    $(fill_to).val(str);
    return str;
}

$(document).ready(function(){
    $(".checkAllDeleteCategory").click(function(){
        $('.checkDeleteCategory').not(this).prop('checked', this.checked);
    });        
}); 



function add_new_poll()
{
    $(document).ready(function(){
        var question = $("input[name=input-create-question]").val();
        var choices = [];
        $('input[name^="input-create-choice"]').each(function() {  
            if($(this).val() != "")
            {          
                choices.push($(this).val());
            }
        });
        var date_range_poll = $('input[name="date_range_poll"]').val();
        date_range_poll = date_range_poll.split(" - ");
        var start_date = date_range_poll[0];
        var end_date = date_range_poll[1];
                        
        if(question != "" && choices.length > 0)
        {
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');      
            
            $.ajax({
                type: "POST",        
                url: '/'+baseUrl + '/news/addnewpoll',
                data: {_token: CSRF_TOKEN, question: question, choices: choices, start_date:start_date, end_date:end_date},    
                error: function (e) {
                    //hideMask();
                },
                success: function (data) {
                    
                    
                    
                   if(data.status == 'success')
                   {
                        $('select[name="polls"]').append($('<option>', {
                            value: data.id_poll,
                            text: question
                        }));
                        $('#modal-default').modal('toggle');
                        alert('Đã thêm thành công!');
                    }
                    else if(data.status == 'exist')
                    {
                        alert('Bạn đã tạo câu thăm dò này rồi!');
                    }
                    
                }
            });
           
        }
        else
        {
            alert("Vui lòng thêm đầy đủ thông tin để tạo thăm dò");
        }        

    });     
}

function doSubmit(form, action) {
	if(confirm('Are you sure?')){            
        form.action = action;        
        form.submit();        
    }
	
}

function changeStatus(id_element,class_status,dir_controller)
{
    $(document).ready(function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');       
        var status = 0;
        if($(class_status).prop('checked') == true){
            status = 1;
        }
        else
        {
            status = 0;
        }
        $.ajax({
            type: "POST",        
            url: '/'+baseUrl + '/'+dir_controller+'/updatestatus',
            data: {_token: CSRF_TOKEN, id: id_element, status: status},    
            error: function (e) {
                //hideMask();
            },
            success: function (data) {
               if(data.status == 'success')
               {
                    alert('Đã thay đổi');
                }
            }
        });
    }); 
    
}

function changeAutoActivePub(id_element,class_status)
{
    $(document).ready(function(){
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');       
        var auto_active_pub = 0;
        if($(class_status).prop('checked') == true){
            auto_active_pub = 1;
        }
        else
        {
            auto_active_pub = 0;
        }
        $.ajax({
            type: "POST",        
            url: '/'+baseUrl + '/pubs/updateautoactivepub',
            data: {_token: CSRF_TOKEN, id: id_element, auto_active_pub: auto_active_pub},    
            error: function (e) {
                //hideMask();
            },
            success: function (data) {
               if(data.status == 'success')
               {
                    alert('Đã thay đổi');
                }
            }
        });
    }); 
    
}

function add_feed()
{
   
	var div1 = document.createElement('div');
	// Get template data
	div1.innerHTML = document.getElementById('newlinktpl').innerHTML;
	// append to our form, so that template data
    //become part of form
    //var div1 = '<input type="text" class="form-control"  name="input-create-answer[]" value="" />';
    //alert(div1);
    document.getElementById('newlink').appendChild(div1);
    
}