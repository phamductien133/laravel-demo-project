<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSocialAccount extends Model
{
    protected $fillable = ['user_id', 'provider_user_id', 'provider', 'provider_url', 'is_register', 'status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
