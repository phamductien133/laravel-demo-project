<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserIdentityCard extends Model
{
    protected $fillable = ['user_id', 'name', 'identity_card_number', 'doi', 'poi', 'tax_id', 'status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
