<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserOccupation extends Model
{
    protected $fillable = ['user_id', 'occupation_id', 'status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
