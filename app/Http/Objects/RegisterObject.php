<?php

namespace App\Http\Objects;


class RegisterObject
{

    public $email;
    public $name;
    public $phone_number;
    public $password;
    public $confirm_password;
    public $dob;
    public $gender;
    public $address;
    public $occupations;
    public $topics;

    public function __construct($data)
    {
        $this->email = $data['email'];
        $this->name = $data['name'];
        $this->phone_number = $data['phone_number'];
        $this->password = $data['password'];
        $this->confirm_password = $data['confirm_password'];
        $this->dob = $data['dob'];
        $this->gender = $data['gender'];
        $this->address = $data['address'];
        $this->occupations = $data['occupation'];
        $this->topics = $data['topic'];
    }

    public function _checkData()
    {
        $hasError = FALSE;
        $message = '';
        if (preg_match("/[\'^£$^@.%&*()}\"{#~?>!<>`~,|=+¬|:;,]/", $this->name)) {
            $hasError = true;
            $message = 'Invalid full name.';
        }
        /*if (!self::_isEmail($this->email)) {
            $hasError = TRUE;
            $message = 'Invalid email';
        }*/
        if (!self::_is_strong_password($this->password)) {
            $hasError = TRUE;
            $message = 'Password should contain at least one lowercase letter, uppercase letter, numeric digit, special character and must be more than 8 characters.';
        }
        $hasSpace = strpos($this->password, ' ');
        if ($hasSpace !== FALSE) {
            $hasError = TRUE;
            $message = 'Password must not contain space.';
        }
        if ($this->password != $this->confirm_password) {
            $hasError = TRUE;
            $message = 'Confirm Password does not match.';
        }
        return [
            'status' => $hasError,
            'message' => $message,
        ];

    }

    private static function _is_strong_password($password) {
        $match_lower = preg_match("/[a-z]+/", $password);
        $match_upper = preg_match("/[A-Z]+/", $password);
        $match_number = preg_match("/[0-9]+/", $password);
        $match_special = preg_match("/[\W]+/", $password);

        $match_valid_success = 0;

        $match_valid_success += ($match_lower) ? 1 : 0;
        $match_valid_success += ($match_upper) ? 1 : 0;
        $match_valid_success += ($match_number) ? 1 : 0;
        $match_valid_success += ($match_special) ? 1 : 0;

        if (strlen($password) < 8) {
            return FALSE;
        }
        else {
            if ($match_valid_success < 4) {
                return FALSE;
            }
        }

        return TRUE;
    }
}