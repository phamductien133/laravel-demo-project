<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\UserSocialAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\DB;

class BackendController extends Controller
{

    public function getAdminLogin()
    {
        if (Auth::check()) {
            return redirect()->to('/back-office');
        } else {
            return view('backend.pages.login');
        }
    }

    public function postAdminLogin(LoginRequest $request)
    {
        $login = [
            'email' => $request->txtEmail,
            'password' => $request->txtPassword,
            'level' => 1,
            'status' => 'active'
        ];
        if (Auth::attempt($login)) {
            return redirect()->to('/back-office');
        } else {
            return redirect()->back()->with('status', 'Invalid Email or Password');
        }
    }

    public function getAdminLogout()
    {
        if (Auth::check()) {
            Auth::logout();
            return redirect()->route('getAdminLogin');
        } else {
            return redirect()->to('/back-office');
        }
    }

    public function dashboard(Request $request)
    {
        if ($request->isMethod('get')) {
            session()->put('forms.name', $request->get('filter_name'));
            session()->put('forms.address', $request->get('filter_address'));
            session()->put('forms.provider', $request->get('filter_provider'));
            session()->put('forms.occupation', $request->get('filter_occupation'));
            session()->put('forms.age_range', $request->get('filter_age_range'));
            session()->put('forms.identity_number', $request->get('filter_identity_number'));
            session()->put('forms.bank_account_number', $request->get('filter_bank_account_number'));
        }
        $occupations = DB::table('occupations')
            ->where('status', 'active')
            ->get();

        $users = DB::table('users')
            ->orderBy('id', 'desc')
            ->where('level', '0');
        $ageRange = [
            1 => 'Under 20',
            2 => 'From 20 - 40',
            3 => 'From 40 - 60',
            4 => 'From 60 - 80',
            5 => 'Over 80',
        ];
        if ($request->get('filter_name')) {
            $users->where('name', 'LIKE', '%' . $request->get('filter_name') . '%');
        }

        if ($request->get('filter_age_range')) {
            $key = $request->get('filter_age_range');
            if ($key == 1) {
                $ageFrom = 0;
                $ageTo = 20;
            } else if ($key == 2) {
                $ageFrom = 20;
                $ageTo = 40;
            } else if ($key == 3) {
                $ageFrom = 40;
                $ageTo = 60;
            } else if ($key == 4) {
                $ageFrom = 60;
                $ageTo = 80;
            } else {
                $ageFrom = 80;
                $ageTo = 1000;
            }
            $today = date("Y/m/d");
            $currentTime = strtotime($today);
            $users->where('dob_timestamp', '<=', $currentTime - ($ageFrom * 31536000));
            $users->where('dob_timestamp', '>', $currentTime - ($ageTo * 31536000));
        }
        if ($request->get('filter_address')) {
            $users->where('address', 'LIKE', '%' . $request->get('filter_address') . '%');
        }
        if ($request->get('filter_identity_number')) {
            $userIdentities = DB::table('user_identity_cards')
                ->where('status', 'active')
                ->where('identity_card_number', $request->get('filter_identity_number'))
                ->get();
            $userIds = [];
            foreach ($userIdentities as $userIdentity) {
                $userIds[] = $userIdentity->user_id;
            }
            $users->whereIn('id', $userIds);
        }
        if ($request->get('filter_bank_account_number')) {
            $userBankAccounts = DB::table('user_bank_accounts')
                ->where('status', 'active')
                ->where('bank_account_id', $request->get('filter_bank_account_number'))
                ->get();
            $userIds = [];
            foreach ($userBankAccounts as $userBankAccount) {
                $userIds[] = $userBankAccount->user_id;
            }
            $users->whereIn('id', $userIds);
        }
        if ($request->get('filter_occupation')) {
            $userOccupations = DB::table('user_occupations')
                ->where('status', 'active')
                ->where('occupation_id', $request->get('filter_occupation'))
                ->get();
            $userIds = [];
            foreach ($userOccupations as $userOccupation) {
                $userIds[] = $userOccupation->user_id;
            }
            $users->whereIn('id', $userIds);
        }
        if ($request->get('filter_provider')) {
            $userProviders = DB::table('user_social_accounts')
                ->where('status', 'active')
                ->where('provider', $request->get('filter_provider'))
                ->get();
            $userIds = [];
            foreach ($userProviders as $userProvider) {
                $userIds[] = $userProvider->user_id;
            }
            $users->whereIn('id', $userIds);
        }

        $users = $users->paginate(10);
        // Get user provider
        foreach ($users as $user) {
            $accounts = UserSocialAccount::whereUserId($user->id)
                ->get();
            $provider = '';
            foreach ($accounts as $account) {
                $provider .= '<p>' . $account->provider . '</p>';
            }
            $user->provider = $provider;
        }

        return view('backend.pages.index',
            [
                'users' => $users,
                'occupations' => $occupations,
                'age_range' => $ageRange,
            ]
        )->with(
            ($request->input('page', 1) - 1) * 10
        );
    }
}
