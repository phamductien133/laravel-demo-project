<?php

namespace App\Http\Controllers;


use App\Http\Objects\RegisterObject;
use App\Http\Requests\IdentityCardRequest;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\PaymentInfoRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use App\UserBankAccount;
use App\UserIdentityCard;
use App\UserOccupation;
use App\UserSocialAccount;
use App\UserTopic;
use DateTime;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class FrontendController extends Controller
{

    public function getProfile()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $data = $user;
            $userIdentityCard = UserIdentityCard::whereUserId($user->id)
                ->first();
            if (!$userIdentityCard) {
                $userIdentityCard = [
                    'name' => '',
                    'identity_card_number' => '',
                    'doi' => '',
                    'poi' => '',
                    'tax_id' => '',
                ];
            }

            $userBankAccount = UserBankAccount::whereUserId($user->id)
                ->first();
            if (!$userBankAccount) {
                $userBankAccount = [
                    'name' => '',
                    'bank_account_id' => '',
                    'bank_name' => '',
                    'branch' => '',
                    'city' => '',
                ];
            }
            $data['title'] = 'User Profile';
            return view('frontend.pages.profile',
                [
                    'data' => $data,
                    'identityCard' => $userIdentityCard,
                    'paymentData' => $userBankAccount,
                ]
            );
        } else {
            return redirect()->route('getLogin');
        }
    }

    public function getLogout()
    {
        if (Auth::check()) {
            Auth::logout();
            return redirect()->to('/');
        } else {
            return redirect()->route('getLogin');
        }
    }

    public function getIndex()
    {
        return redirect()->route('getLogin');
    }

    public function getSocialRegister()
    {
        $data = [];
        $data['title'] = 'Social Register';

        if (Auth::check()) {
            return redirect()->to('/profile');
        } else {
            return view('frontend.pages.social_register', ['data' => $data]);
        }
    }

    public function getRegister()
    {
        $requestData = Session::get('data');
        $checkEmail = Session::get('check', 1);
        $message = Session::get('message');
        if (!$requestData) {
            return redirect()->route('getSocialRegister');
        }
        if (!$checkEmail) {
            $requestData['email'] = '';
        }
        $occupations = DB::table('occupations')
            ->where('status', 'active')
            ->get();

        $topics = DB::table('topics')
            ->where('status', 'active')
            ->get();

        if (Auth::check()) {
            return redirect()->to('/profile');
        } else {
            $requestData['title'] = 'Register';
            return view(
                'auth.register',
                [
                    'data' => $requestData,
                    'status' => $message,
                    'occupations' => $occupations,
                    'topics' => $topics,
                    'check' => $checkEmail,
                ]
            );
        }
    }

    public function postRegister(RegisterRequest $request)
    {
        $formData = [
            'email' => $request->email,
            'name' => $request->name,
            'phone_number' => $request->phoneNumber,
            'password' => $request->password,
            'confirm_password' => $request->password_confirm,
            'dob' => $request->dob,
            'gender' => $request->gender,
            'address' => $request->address,
            'occupation' => $request->occupation,
            'topic' => $request->topic,
            'check' => $request->check,
            'provider_id' => $request->provider_id,
        ];
        $registerObject = new RegisterObject($formData);
        $hasError = $registerObject->_checkData();
        if ($hasError['status']) {
            return redirect()->back()->with(['message' => $hasError['message'], 'data' => $formData]);
        }
        $gender = $registerObject->gender;
        $user = User::whereEmail($registerObject->email)->first();

        // Do not have email
        if (!$request->check) {
            if ($user) {
                return redirect()->back()->with(['message' => 'Email already exists', 'data' => $formData]);
            } else {
                if (!self::_isEmail($registerObject->email)) {
                    return redirect()->back()->with(['message' => 'Invalid Email', 'data' => $formData]);
                }
                $user = User::whereEmail($formData['provider_id'])->first();
                if ($user) {
                    $user->email = $registerObject->email;
                    $user->name = $registerObject->name;
                    $user->password = Hash::make($registerObject->password);
                    $user->phone_number = $registerObject->phone_number;
                    $user->dob = $registerObject->dob;
                    $dtime = DateTime::createFromFormat("Y-m-d", $registerObject->dob);
                    $timestamp = $dtime->getTimestamp();
                    $user->dob_timestamp = $timestamp;
                    $user->gender = $gender;
                    $user->address = $registerObject->address ? $registerObject->address : '';
                    $user->save();
                    $account = UserSocialAccount::whereUserId($user->id)
                        ->first();
                    if ($account) {
                        $account->is_register = 1;
                        $account->save();
                    }
                    $occupations = $registerObject->occupations;
                    if (isset($occupations)) {
                        foreach ($occupations as $occupationId => $value) {
                            $occupation = new UserOccupation(
                                [
                                    'occupation_id' => $occupationId,
                                    'status' => 'active'
                                ]
                            );

                            $occupation->user()->associate($user);
                            $occupation->save();
                        }
                    }
                    $topics = $registerObject->topics;
                    if (isset($topics)) {
                        foreach ($topics as $topicId => $value) {
                            $topic = new UserTopic(
                                [
                                    'topic_id' => $topicId,
                                    'status' => 'active'
                                ]
                            );

                            $topic->user()->associate($user);
                            $topic->save();
                        }
                    }
                    auth()->login($user);
                }
            }
        } else {
            if ($user) {
                $user->name = $registerObject->name;
                $user->password = Hash::make($registerObject->password);
                $user->phone_number = $registerObject->phone_number;
                $user->dob = $registerObject->dob;
                $dtime = DateTime::createFromFormat("Y-m-d", $registerObject->dob);
                $timestamp = $dtime->getTimestamp();
                $user->dob_timestamp = $timestamp;
                $user->gender = $gender;
                $user->address = $registerObject->address ? $registerObject->address : '';
                $user->save();
                $account = UserSocialAccount::whereUserId($user->id)
                    ->first();
                if ($account) {
                    $account->is_register = 1;
                    $account->save();
                }
                $occupations = $registerObject->occupations;
                if (isset($occupations)) {
                    foreach ($occupations as $occupationId => $value) {
                        $occupation = new UserOccupation(
                            [
                                'occupation_id' => $occupationId,
                                'status' => 'active'
                            ]
                        );

                        $occupation->user()->associate($user);
                        $occupation->save();
                    }
                }
                $topics = $registerObject->topics;
                if (isset($topics)) {
                    foreach ($topics as $topicId => $value) {
                        $topic = new UserTopic(
                            [
                                'topic_id' => $topicId,
                                'status' => 'active'
                            ]
                        );

                        $topic->user()->associate($user);
                        $topic->save();
                    }
                }
                auth()->login($user);
            }
        }

        $sendEmail = $registerObject->email;
        Mail::send('frontend.emails.index', array('email' => $sendEmail), function ($message) use ($sendEmail) {
            $message->to($sendEmail, '')->subject('Welcome to Hiip!');
        });

        return redirect()->to('/profile');
    }

    public function getLogin()
    {
        $data = [];
        $data['title'] = 'Login';
        if (Auth::check()) {
            return redirect()->to('/profile');
        } else {
            return view('frontend.pages.login', ['data' => $data]);
        }
    }

    public function postLogin(LoginRequest $request)
    {
        $login = [
            'email' => $request->txtEmail,
            'password' => $request->txtPassword,
            'level' => 0,
            'status' => 'active'
        ];
        if (Auth::attempt($login)) {
            return redirect()->to('/profile');
        } else {
            return redirect()->back()->with('status', 'Invalid Email or Password');
        }
    }

    public function postIdentityCard(IdentityCardRequest $request)
    {
        if (!Auth::check()) {
            return redirect()->to('/login');
        } else {
            $identityCard = [
                'name' => $request->txtName,
                'number' => $request->number,
                'doi' => $request->doi,
                'poi' => $request->poi,
                'tax_id' => $request->taxId,
                'status' => 'active',
            ];
            $user = Auth::user();

            $userIdentityCard = UserIdentityCard::whereUserId($user->id)
                ->first();
            if ($userIdentityCard) {
                $userIdentityCard->name = $identityCard['name'];
                $userIdentityCard->identity_card_number = $identityCard['number'];
                $userIdentityCard->doi = $identityCard['doi'];
                $userIdentityCard->poi = $identityCard['poi'];
                $userIdentityCard->tax_id = $identityCard['tax_id'];
            } else {
                $userIdentityCard = new UserIdentityCard([
                    'name' => $identityCard['name'],
                    'identity_card_number' => $identityCard['number'],
                    'doi' => $identityCard['doi'],
                    'poi' => $identityCard['poi'],
                    'tax_id' => $identityCard['tax_id'],
                    'status' => $identityCard['status'],
                ]);
                $userIdentityCard->user()->associate($user);
            }
            $userIdentityCard->save();
            return redirect()->to('/profile');
        }

    }

    public function postPaymentInfo(PaymentInfoRequest $request)
    {
        if (!Auth::check()) {
            return redirect()->to('/login');
        } else {
            $paymentData = [
                'name' => $request->txtName,
                'number' => $request->number,
                'bank_name' => $request->bankName,
                'branch' => $request->branch,
                'city' => $request->city,
                'status' => 'active',
            ];
            $user = Auth::user();

            $userBankAccount = UserBankAccount::whereUserId($user->id)
                ->first();
            if ($userBankAccount) {
                $userBankAccount->name = $paymentData['name'];
                $userBankAccount->bank_account_id = $paymentData['number'];
                $userBankAccount->bank_name = $paymentData['bank_name'];
                $userBankAccount->branch = $paymentData['branch'];
                $userBankAccount->city = $paymentData['city'];
            } else {
                $userBankAccount = new UserBankAccount([
                    'name' => $paymentData['name'],
                    'bank_account_id' => $paymentData['number'],
                    'bank_name' => $paymentData['bank_name'],
                    'branch' => $paymentData['branch'],
                    'city' => $paymentData['city'],
                    'status' => $paymentData['status'],
                ]);
                $userBankAccount->user()->associate($user);
            }
            $userBankAccount->save();
            return redirect()->to('/profile');
        }

    }

    private static function _isEmail($str)
    {
        $isEmail = TRUE;
        // Remove all illegal characters from email
        $str = filter_var($str, FILTER_SANITIZE_EMAIL);

        // Validate e-mail
        if (!filter_var($str, FILTER_VALIDATE_EMAIL) === FALSE) {
            if (strlen($str) > 254) {
                $isEmail = FALSE;
            } else {
                $tmp = explode('@', $str);
                if (strlen($tmp[0]) > 64 || strlen($tmp[1]) > 188) {
                    $isEmail = FALSE;
                }
            }
        } else {
            $isEmail = FALSE;
        }
        if (preg_match("/[\'^£$^%&*()}\"{#~?>!<>`~,|=+¬|:;,]/", $str)) {
            $isEmail = FALSE;
        }
        return $isEmail;
    }
}
