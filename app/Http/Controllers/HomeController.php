<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->route('getLogin');
    }

    public function postRegister(RegisterRequest $request)
    {
        return redirect()->route('getProfile');
    }

    public function getLogin()
    {
        $data = [];
        $data['title'] = 'Login';
        if (Auth::check()) {
            return redirect()->route('getProfile');
        } else {
            return view('frontend.pages.login', ['data' => $data]);
        }
    }

    public function postLogin(LoginRequest $request)
    {
        $login = [
            'email' => $request->txtEmail,
            'password' => $request->txtPassword,
            'level' => 0,
            'status' => 'active'
        ];
        if (Auth::attempt($login)) {
            return redirect()->route('getProfile');
        } else {
            return redirect()->back()->with('status', 'Invalid Email or Password');
        }
    }

    public function getProfile()
    {
        $data = [];
        $data['title'] = 'User Profile';
        return view('frontend.pages.profile', ['data' => $data]);
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->to('/');
    }
}
