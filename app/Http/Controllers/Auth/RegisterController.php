<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\UserOccupation;
use App\UserSocialAccount;
use App\UserTopic;
use DateTime;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Http\Requests;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phoneNumber' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $gender = isset($data['gender']) ? $data['gender'] : 'other';
        $user = User::whereEmail($data['email'])->first();
        if ($user) {
            $user->name =  $data['name'];
            $user->password =  self::_encryptSaltPassword($data['password']);
            $user->phone_number = $data['phone_number'];
            $user->dob = $data['dob'];
            $dtime = DateTime::createFromFormat("Y-m-d", $data['dob']);
            $timestamp = $dtime->getTimestamp();
            $user->dob_timestamp = $timestamp;
            $user->gender = $gender;
            $user->address = isset($data['address']) ? $data['address'] : '';
            $user->save();
            $account = UserSocialAccount::whereUserId($user->getId())
                ->first();
            if ($account) {
                $account->is_register = 1;
                $account->save();
            }
            if (isset($data['occupation'])) {
                foreach ($data['occupation'] as $occupationId => $value) {
                    $occupation = new UserOccupation(
                        [
                            'occupation_id' => $occupationId,
                            'status' => 'active'
                        ]
                    );

                    $occupation->user()->associate($user);
                    $occupation->save();
                }
            }
            if (isset($data['topic'])) {
                foreach ($data['topic'] as $topicId => $value) {
                    $topic = new UserTopic(
                        [
                            'topic_id' => $topicId,
                            'status' => 'active'
                        ]
                    );

                    $topic->user()->associate($user);
                    $topic->save();
                }
            }
        }
        /*$user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => self::_encryptSaltPassword($data['password']),
            'phone_number' => isset($data['phoneNumber']) ? $data['phoneNumber'] : '',
            'dob' => isset($data['dob']) ? $data['dob'] : date('Y-m-d'),
            'gender' => $gender,
            'address' => isset($data['address']) ? $data['address'] : '',
            'level' => 0,
            'status' => 'active',
        ]);*/

//        auth()->login($user);
        return $user;

    }

    private static function _encryptSaltPassword($password)
    {
        $options = ['cost' => 12];

        return password_hash($password, PASSWORD_DEFAULT, $options);
    }
}
