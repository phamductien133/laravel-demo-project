<?php

namespace App\Http\Controllers;

use App\User;
use App\UserSocialAccount;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Services\SocialAccountService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Socialite;
use SocialiteProviders;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialAuthController extends Controller
{
    public function redirect($social)
    {
        if ($social != 'instagram') {
            return Socialite::driver($social)->redirect();
        } else {
            return Socialite::with($social)->redirect();
        }
    }

    public function callback(Request $request, $social)
    {
        if ($social != 'instagram') {
            $providerUser = Socialite::driver($social)->user();
        } else {
            $providerUser = Socialite::with($social)->user();
        }
        $account = UserSocialAccount::whereProvider($social)
            ->whereProviderUserId($providerUser->getId())
            ->first();
        $formData = [
            'email' => '',
            'name' => '',
            'phone_number' => '',
            'password' => '',
            'confirm_password' => '',
            'dob' => '',
            'gender' => '',
            'address' => ''
        ];
        if ($account) {
            if ($account->is_register == 0) {
                $user = $account->user;
                $formData['email'] = $user->email;
                $formData['provider_id'] = $account->provider_user_id;
                $check = self::_isEmail($formData['email']);
                return redirect()->route('getRegister')->with(['data' => $formData, 'check' => $check]);
//                return view('auth.register');
            } else {
                $user = $account->user;
                auth()->login($user);
                return redirect()->route('getProfile')->with(['data' => $account]);
            }
        } else {
            $account = self::_handleAccountInformation($providerUser, $social);
            $user = $account->user;
            $formData['email'] = $user->email;
            if ($account->is_register == 1) {
                auth()->login($user);
                return redirect()->to('/profile')->with(['data' => $account]);
            } else {
                $check = self::_isEmail($formData['email']);
                $formData['provider_id'] = $account->provider_user_id;
                return redirect()->route('getRegister')->with(['data' => $formData, 'check' => $check]);
            }
        }
    }

    private static function _handleAccountInformation(ProviderUser $providerUser, $social)
    {
        if ($social != 'instagram') {
            $email = isset($providerUser['email']) ? $providerUser['email'] : $providerUser->getId();
            $name = $providerUser['name'] ? $providerUser['name'] : '';
            $profileUrl = $providerUser->user['link'] ? $providerUser->user['link'] : '';
        } else {
            $userData = $providerUser->user;
            $email = $providerUser->getId();
            $name = $userData['full_name'];
            $profileUrl = $userData['website'];
        }
        $user = User::whereEmail($email)->first();
        $isRegister = 0;
        if (!$user) {
            $user = User::create([
                'name' => $name,
                'email' => $email,
                'level' => 0,
                'status' => 'active',
            ]);
            $user->save();
        } else {
            $sendEmail = $email;
            Mail::send('frontend.emails.index', array('email' => $sendEmail), function ($message) use ($sendEmail) {
                $message->to($sendEmail, '')->subject('Welcome to Hiip!');
            });
            $isRegister = 1;
        }

        $account = new UserSocialAccount([
            'provider_user_id' => $providerUser->getId(),
            'provider' => $social,
            'provider_url' => $profileUrl,
            'status' => 'active',
            'is_register' => $isRegister
        ]);

        $account->user()->associate($user);
        $account->save();
        return $account;
    }

    private static function _isEmail($str)
    {
        $isEmail = TRUE;
        // Remove all illegal characters from email
        $str = filter_var($str, FILTER_SANITIZE_EMAIL);

        // Validate e-mail
        if (!filter_var($str, FILTER_VALIDATE_EMAIL) === FALSE) {
            if (strlen($str) > 254) {
                $isEmail = FALSE;
            } else {
                $tmp = explode('@', $str);
                if (strlen($tmp[0]) > 64 || strlen($tmp[1]) > 188) {
                    $isEmail = FALSE;
                }
            }
        } else {
            $isEmail = FALSE;
        }
        if (preg_match("/[\'^£$^%&*()}\"{#~?>!<>`~,|=+¬|:;,]/", $str)) {
            $isEmail = FALSE;
        }
        return $isEmail;
    }
}
