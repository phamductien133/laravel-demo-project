<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'phoneNumber' => 'required',
            'password' => 'required',
            'password_confirm' => 'required',
            'dob' => 'required',
            'gender' => 'required',
            'address' => 'required',
        ];
    }

    /**
     * customize msg error
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'phoneNumber.required' => 'Phone number is required',
            'password.required' => 'Password is required',
            'password-confirm.required' => 'Password Confirm is required',
            'dob.required' => 'DOB is required',
            'gender.required' => 'Gender is required',
            'address.required' => 'Address is required',
        ];
    }
}

?>