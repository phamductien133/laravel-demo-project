<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IdentityCardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtName' => 'required',
            'number' => 'required',
            'doi' => 'required',
            'poi' => 'required',
            'taxId' => 'required',
        ];
    }

    /**
     * customize msg error
     * @return array
     */
    public function messages()
    {
        return [
            'txtName.required' => 'Name is required',
            'number.required' => 'number is required',
            'doi.required' => 'DOI is required',
            'poi.required' => 'POI is required',
            'taxId.required' => 'Tax id is required',
        ];
    }
}

?>