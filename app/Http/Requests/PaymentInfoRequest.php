<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'txtName' => 'required',
            'number' => 'required',
            'city' => 'required',
            'bankName' => 'required',
            'branch' => 'required',
        ];
    }

    /**
     * customize msg error
     * @return array
     */
    public function messages()
    {
        return [
            'txtName.required' => 'Name is required',
            'number.required' => 'number is required',
            'city.required' => 'City is required',
            'bankName.required' => 'Bank name is required',
            'branch.required' => 'Branch id is required',
        ];
    }
}

?>