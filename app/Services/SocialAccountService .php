<?php

namespace App\Services;

use Laravel\Socialite\Contracts\User as ProviderUser;
use App\UserSocialAccount;
use App\User;

class SocialAccountService
{
    public static function createOrGetUser(ProviderUser $providerUser, $social)
    {
        $name = $providerUser->getName() ? $providerUser->getName() : 'name';
        $email = $providerUser->getEmail() ? $providerUser->getEmail() : 'email';
        $password = self::_encryptSaltPassword('password');
        $phoneNumber = '000000000';
        $dob = date('Y-m-d', time());
        $gender = 'gender';
        $address = 'address';

        $account = UserSocialAccount::whereProvider($social)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $account = new UserSocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $social
            ]);
            $user = User::whereEmail($email)->first();

            if (!$user) {
                $user = User::create([
                    'name' => $name,
                    'email' => $email,
                    'password' => $password,
                    'phone_number' => $phoneNumber,
                    'dob' => $dob,
                    'gender' => $gender,
                    'address' => $address,
                    'status' => 'inactive',
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }

    private static function _encryptSaltPassword($password)
    {
        $options = ['cost' => 12];

        return password_hash($password, PASSWORD_DEFAULT, $options);
    }
}