<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTopic extends Model
{
    protected $fillable = ['user_id', 'topic_id', 'status'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
