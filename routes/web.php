<?php
//********** FRONTEND **********
Route::get('/', 'FrontendController@getIndex');

Route::get('/redirect/{social}', 'SocialAuthController@redirect');

Route::get('/callback/{social}', 'SocialAuthController@callback');

Route::get('/social-register', ['as' => 'getSocialRegister', 'uses' => 'FrontendController@getSocialRegister']);

Route::get('/logout', ['as' => 'getLogout', 'uses' => 'FrontendController@getLogout']);

Route::get('/register', ['as' => 'getRegister', 'uses' => 'FrontendController@getRegister']);

Route::post('/register', ['as' => 'postRegister', 'uses' => 'FrontendController@postRegister']);

Route::get('/login', ['as' => 'getLogin', 'uses' => 'FrontendController@getLogin']);

Route::post('/login', ['as' => 'postLogin', 'uses' => 'FrontendController@postLogin']);

Route::get('/profile', ['as' => 'getProfile', 'uses' => 'FrontendController@getProfile']);

Route::post('/post-identity-card', ['as' => 'postIdentityCard', 'uses' => 'FrontendController@postIdentityCard']);
Route::post('/post-payment-info', ['as' => 'postPaymentInfo', 'uses' => 'FrontendController@postPaymentInfo']);

//********** BACKEND **********
Route::group(['prefix' => 'back-office'], function () {
    Route::get('/login', ['as' => 'getAdminLogin', 'uses' => 'BackendController@getAdminLogin']);
    Route::post('/login', ['as' => 'postAdminLogin', 'uses' => 'BackendController@postAdminLogin']);
    Route::get('/logout', ['as' => 'getAdminLogout', 'uses' => 'BackendController@getAdminLogout']);
});

Route::group(['middleware' => 'CheckAdminLogin', 'prefix' => 'back-office'], function () {
    Route::get('/', ['as' => 'dashboard', 'uses' => 'BackendController@dashboard']);
});