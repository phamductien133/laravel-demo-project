@extends('backend.master')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Users</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <form class="form-inline" action="" method="get" role="form" id="form_filter">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Name" style="width: 200px;"
                                           name="filter_name" id="title" value="{{session('forms.name')}}">
                                </div>
                                <div class="form-group">
                                    <select class="form-control select2" name="filter_age_range" style="width: 200px;">
                                        <option value="" selected="selected">--Choose Age Range--</option>
                                        @foreach($age_range as $key => $item)
                                            <option @if( session('forms.age_range')  == $key) selected="selected" @endif value="{{$key}}">{{$item}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Address" style="width: 200px;"
                                           name="filter_address" id="address" value="{{session('forms.address')}}">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Identity Number" style="width: 200px;"
                                           name="filter_identity_number" id="identity_number" value="{{session('forms.identity_number')}}">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Bank Account Number" style="width: 200px;"
                                           name="filter_bank_account_number" id="bank_account_number" value="{{session('forms.bank_account_number')}}">
                                </div>
                                <div class="form-group">
                                    <select class="form-control select2" name="filter_provider" style="width: 200px;">
                                        <option value="" selected="selected">--Choose Provider--</option>
                                        <option
                                                @if( session('forms.provider')  == 'facebook') selected="selected"
                                                @endif value="facebook">Facebook
                                        </option>
                                        <option
                                                @if( session('forms.provider')  == 'instagram') selected="selected"
                                                @endif value="instagram">Instagram
                                        </option>
                                        <option
                                                @if( session('forms.provider')  == 'google') selected="selected"
                                                @endif value="google">Google
                                        </option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <select class="form-control select2" name="filter_occupation" style="width: 200px;">
                                        <option value="" selected="selected">--Choose Occupation--</option>
                                        @foreach($occupations as $occupation)
                                            <option @if( session('forms.occupation')  == $occupation->id) selected="selected" @endif value="{{$occupation->id}}">{{$occupation->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div style="clear:both;margin:10px">
                                    <button class="btn btn-default">
                                        <i class="fa fa-search"></i> Search
                                    </button>&nbsp;&nbsp;
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="row">
                        <form method="post" action="">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}"/>

                            <div class="col-lg-12">

                                <table id="table_news" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>
                                        <th>DOB</th>
                                        <th>Gender</th>
                                        <th>Address</th>
                                        <th>Provider</th>
                                        <th>Created at</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $item)
                                        <tr>
                                            <td>{{$item->id}}</td>
                                            <td>{{$item->name}}</td>
                                            <td>{{$item->email}}</td>
                                            <td>{{$item->phone_number}}</td>
                                            <td>{{$item->dob}}</td>
                                            <td>{{$item->gender}}</td>
                                            <td>{{$item->address}}</td>
                                            <td>{!! $item->provider !!}</td>
                                            <td>{{$item->created_at}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $users->render() !!}
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-body -->

            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
@endsection