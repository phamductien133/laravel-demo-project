<!-- jQuery 3 -->
<script src="{{ asset('public/backend/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('public/backend/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('public/backend/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('public/backend/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('public/backend/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('public/backend/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('public/backend/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('public/backend/bower_components/morris.js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('public/backend/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>

<!-- jvectormap -->
<script src="{{ asset('public/backend/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('public/backend/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('public/backend/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('public/backend/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('public/backend/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('public/backend/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- datetimepicker -->
<script src="{{ asset('public/backend/bower_components/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('public/backend/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('public/backend/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('public/backend/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('public/backend/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('ajax_upload_images/vendor/jquery.ui.widget.js') }}"></script>
<script src="{{ asset('ajax_upload_images/jquery.iframe-transport.js') }}"></script>
<script src="{{ asset('ajax_upload_images/jquery.fileupload.js') }}"></script>
<script src="{{ asset('public/backend/myscript.js') }}"></script>
<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) 
<script src="{{ asset('public/backend/dist/js/pages/dashboard.js') }}"></script>
<script src="{{ asset('public/backend/dist/js/demo.js') }}"></script>
-->
<script>
        if($('#editor1').length > 0)
        {
          CKEDITOR.replace( 'editor1', {
              filebrowserBrowseUrl: '{{ asset('ckfinder/ckfinder.html') }}',
              filebrowserImageBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Images') }}',
              filebrowserFlashBrowseUrl: '{{ asset('ckfinder/ckfinder.html?type=Flash') }}',
              filebrowserUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files') }}',
              filebrowserImageUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images') }}',
              filebrowserFlashUploadUrl: '{{ asset('ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash') }}'
          } );
        }
        $(function () {

                        
            $('.select2').select2();

            $.ajaxSetup({
              headers: {
                'X-CSRF-TOKEN': $('meta[name=”csrf-token”]').attr('content')
              }
            });

            
            $('#table_category').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,              
              'info'        : true,
              'autoWidth'   : false,
              'ordering'    : false,
            })

            $('#table_news').DataTable({
              'paging'      : false,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : false,
              'info'        : false,
              'autoWidth'   : false
            })
            
            $('#fileupload').fileupload({
                dataType: 'json',
                add: function (e, data) {                    
                    $('#loading').text('Uploading...');
                    data.submit();
                },
                done: function (e, data) {                                        
                    $.each(data.result.files, function (index, file) {
                        //$('<p/>').html(file.name + ' (' + file.size + ' KB)').appendTo($('#files_list'));
                        //html_ += "<li>";
                        //html_ += "<img width='200px' src='/uploads/news/"+file.name+"' />";
                        //html_ += "</li>";
                        $("#files_list ul").html("<li><img width='200px' src='/uploads/news/"+file.name+"' /></li>");
                        /*
                        if ($('#file_ids').val() != '') {
                            $('#file_ids').val($('#file_ids').val() + ',');
                        }                        
                        $('#file_ids').val($('#file_ids').val() + file.fileID);
                        */                       
                        $('#file_ids').val(file.fileID);
                    });                                        
                    $('#loading').text('');
                }
            });
            
        })

        $("#datepicker").datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        }).datepicker("setDate", "0");

        $('#published_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',                
        });


        $("#datepicker-edit").datepicker({
          format: "yyyy-mm-dd",
          autoclose: true
        });
       
        $('#date_range_poll').daterangepicker({
          locale: {
            format: 'YYYY/MM/DD'
          },           
           autoclose: true
        });
       


</script>