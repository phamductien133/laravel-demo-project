@extends('frontend/master')
@section('pageTitle', $data['title'])
@section('content')
    <section class="g-mb-100">
        <div class="container">
            <div class="row">
                <!-- Profle Content -->
                <div class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="nav nav-justified u-nav-v1-1 u-nav-primary g-brd-bottom--md g-brd-bottom-2 g-brd-primary g-mb-20"
                        role="tablist" data-target="nav-1-1-default-hor-left-underline"
                        data-tabs-mobile-type="slide-up-down"
                        data-btn-classes="btn btn-md btn-block rounded-0 u-btn-outline-primary g-mb-20">
                        <li class="nav-item">
                            <a class="nav-link g-py-10 active" data-toggle="tab"
                               href="#nav-1-1-default-hor-left-underline--1" role="tab">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link g-py-10" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--2"
                               role="tab">Identity Card Settings</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link g-py-10" data-toggle="tab" href="#nav-1-1-default-hor-left-underline--3"
                               role="tab">Payment Settings</a>
                        </li>
                    </ul>
                    <!-- End Nav tabs -->

                    <!-- Tab panes -->
                    <div id="nav-1-1-default-hor-left-underline" class="tab-content">
                        <!-- Edit Profile -->
                        <div class="tab-pane fade show active" id="nav-1-1-default-hor-left-underline--1"
                             role="tabpanel">
                            <p>Below are name, email addresse, contacts and more on file for your account.</p>
                            <ul class="list-unstyled g-mb-30">
                                <!-- Name -->
                                <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                    <div class="g-pr-10">
                                        <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Name</strong>
                                        <span class="align-top">{{$data['name']}}</span>
                                    </div>
                                    <span>
                        
                      </span>
                                </li>
                                <!-- End Name -->

                                <!-- Your ID -->
                                <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                    <div class="g-pr-10">
                                        <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Email</strong>
                                        <span class="align-top">{{$data['email']}}</span>
                                    </div>
                                    <span>
                        
                      </span>
                                </li>
                                <!-- End Your ID -->

                                <!-- Company Name -->
                                <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                    <div class="g-pr-10">
                                        <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Phone
                                            number</strong>
                                        <span class="align-top">{{$data['phone_number']}}</span>
                                    </div>
                                    <span>
                        
                      </span>
                                </li>
                                <!-- End Company Name -->

                                <!-- Position -->
                                <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                    <div class="g-pr-10">
                                        <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Date
                                            of birth</strong>
                                        <span class="align-top">{{$data['dob']}}</span>
                                    </div>
                                    <span>
                        
                      </span>
                                </li>
                                <!-- End Position -->

                                <!-- Primary Email Address -->
                                <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                    <div class="g-pr-10">
                                        <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Gender</strong>
                                        <span class="align-top">{{$data['gender']}}</span>
                                    </div>
                                    <span>
                        
                      </span>
                                </li>
                                <!-- End Primary Email Address -->

                                <!-- Primary Email Address -->
                                <li class="d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15">
                                    <div class="g-pr-10">
                                        <strong class="d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10">Address</strong>
                                        <span class="align-top">{{$data['address']}}</span>
                                    </div>
                                    <span>

                      </span>
                                </li>
                                <!-- End Primary Email Address -->
                            </ul>
                        </div>
                        <!-- End Edit Profile -->

                        <!-- Security Settings -->
                        <div class="tab-pane fade" id="nav-1-1-default-hor-left-underline--2" role="tabpanel">
                            @if (count($errors) >0)
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li class="text-danger"> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            @if (session('status'))
                                <ul>
                                    <li class="text-danger"> {{ session('status') }}</li>
                                </ul>
                            @endif
                            <form method="post" action="{{ route('postIdentityCard') }}">
                                @csrf
                                <div class="form-group row g-mb-25">
                                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Name</label>
                                    <div class="col-sm-9">
                                        <div class="input-group g-brd-primary--focus">
                                            <input id="txtName" name="txtName"
                                                   class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                   placeholder="Name"
                                                   value="{{$identityCard['name']}}">
                                            <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row g-mb-25">
                                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">ID
                                        Card Number</label>
                                    <div class="col-sm-9">
                                        <div class="input-group g-brd-primary--focus">
                                            <input id="number" name="number"
                                                   class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                   placeholder="ID Card Number"
                                                   value="{{$identityCard['identity_card_number']}}">
                                            <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row g-mb-25">
                                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">DOI</label>
                                    <div class="col-sm-9">
                                        <div class="input-group g-brd-primary--focus">
                                            <input id="doi" name="doi"
                                                   class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                   placeholder="DOI"
                                                   value="{{$identityCard['doi']}}">
                                            <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row g-mb-25">
                                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">POI</label>
                                    <div class="col-sm-9">
                                        <div class="input-group g-brd-primary--focus">
                                            <input id="poi" name="poi"
                                                   class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                   placeholder="POI"
                                                   value="{{$identityCard['poi']}}">
                                            <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row g-mb-25">
                                    <label class="col-sm-3 col-form-label g-color-gray-dark-v2 g-font-weight-700 text-sm-right g-mb-10">Tax
                                        ID</label>
                                    <div class="col-sm-9">
                                        <div class="input-group g-brd-primary--focus">
                                            <input id="taxId" name="taxId"
                                                   class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                   placeholder="Tax ID"
                                                   value="{{$identityCard['tax_id']}}">
                                            <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr class="g-brd-gray-light-v4 g-my-25">

                                <div class="text-sm-right">
                                    <button class="btn u-btn-primary rounded-0 g-py-12 g-px-25" type="submit">Save
                                        Changes
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!-- End Security Settings -->

                        <!-- Payment Options -->
                        <div class="tab-pane fade" id="nav-1-1-default-hor-left-underline--3" role="tabpanel">
                            <h2 class="h4 g-font-weight-300">Manage your Payment Settings</h2>
                            @if (count($errors) >0)
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li class="text-danger"> {{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            @if (session('status'))
                                <ul>
                                    <li class="text-danger"> {{ session('status') }}</li>
                                </ul>
                            @endif
                            <form method="post" action="{{ route('postPaymentInfo') }}">
                                @csrf
                                <div class="row">
                                    <!-- Card Name -->
                                    <div class="col-md-6">
                                        <div class="form-group g-mb-20">
                                            <label class="g-color-gray-dark-v2 g-font-weight-700 g-mb-10"
                                                   for="inputGroup1_1">Name on card</label>
                                            <div class="input-group g-brd-primary--focus">
                                                <input id="txtName"
                                                       name="txtName"
                                                       class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                       type="text" placeholder="John Doe"
                                                       value="{{$paymentData['name']}}">
                                                <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                                    <i class="icon-user"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Card Name -->
                                    <!-- Card Number -->
                                    <div class="col-md-6">
                                        <div class="form-group g-mb-20">
                                            <label class="g-color-gray-dark-v2 g-font-weight-700 g-mb-10"
                                                   for="inputGroup1_1">Card number</label>
                                            <div class="input-group g-brd-primary--focus">
                                                <input id="number"
                                                       name="number"
                                                       class="form-control form-control-md g-brd-right-none rounded-0 g-py-13"
                                                       type="text" placeholder="XXXX-XXXX-XXXX-XXXX"
                                                       data-mask="9999-9999-9999-9999"
                                                       value="{{$paymentData['bank_account_id']}}">
                                                <div class="input-group-addon d-flex align-items-center g-color-gray-dark-v5 rounded-0">
                                                    <i class="icon-credit-card"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End Card Number -->
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group g-mb-20">
                                            <label class="g-color-gray-dark-v2 g-font-weight-700 g-mb-10"
                                                   for="inputGroup1_1">City</label>
                                            <div class="input-group g-brd-primary--focus">
                                                <input id="city"
                                                       name="city"
                                                       class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                       type="text" placeholder="City"
                                                       value="{{$paymentData['city']}}">
                                                <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group g-mb-20">
                                            <label class="g-color-gray-dark-v2 g-font-weight-700 g-mb-10"
                                                   for="inputGroup1_1">Bank Name</label>
                                            <div class="input-group g-brd-primary--focus">
                                                <input id="bankName"
                                                       name="bankName"
                                                       class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                       type="text" placeholder="Bank Name"
                                                       value="{{$paymentData['bank_name']}}">
                                                <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group g-mb-20">
                                            <label class="g-color-gray-dark-v2 g-font-weight-700 g-mb-10"
                                                   for="inputGroup1_1">Branch</label>
                                            <div class="input-group g-brd-primary--focus">
                                                <input id="branch"
                                                       name="branch"
                                                       class="form-control form-control-md border-right-0 rounded-0 g-py-13 pr-0"
                                                       type="text" placeholder="Branch"
                                                       value="{{$paymentData['branch']}}">
                                                <div class="input-group-addon d-flex align-items-center g-bg-white g-color-gray-light-v1 rounded-0">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    </div>
                                </div>

                                <hr class="g-brd-gray-light-v4 g-my-25">
                                <div class="text-sm-right">
                                    <button class="btn u-btn-primary rounded-0 g-py-12 g-px-25" type="submit">Save
                                        Changes
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!-- End Payment Options -->
                    </div>
                    <!-- End Tab panes -->
                </div>
                <!-- End Profle Content -->
            </div>
        </div>
    </section>
@endsection