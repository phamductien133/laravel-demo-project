@extends('frontend/master')
@section('pageTitle', $data['title'])
@section('content')
    <!-- Signup -->
    <section class="container g-py-100">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-md-9 col-lg-6">
                <div class="g-brd-around g-brd-gray-light-v4 rounded g-py-40 g-px-30">
                    @if ($data['check'] == 1)
                        @if (count($errors) >0)
                            <ul>
                                @foreach($errors->all() as $error)
                                    <li class="text-danger"> {{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif
                        @if (session('status'))
                            <ul>
                                <li class="text-danger"> {{ session('status') }}</li>
                            </ul>
                        @endif
                        <form class="g-py-15" action="{{ route('postRegister') }}" method="post">
                            {{ csrf_field() }}
                            <div class="mb-4">
                                <label for="example-full-name-input" class="col-form-label">Full Name</label>
                                <input name="txtFullName"
                                       class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                       type="text" placeholder="Full Name">
                            </div>
                            <div class="mb-4">
                                <label for="example-phone-number-input" class="col-form-label">Phone Number</label>
                                <input name="txtPhoneNumber"
                                       class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                       type="text" placeholder="Phone Number">
                            </div>
                            <div class="mb-4">
                                <label for="example-email-input" class="col-form-label">Email</label>
                                <input name="txtEmail"
                                       class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                       type="email" placeholder="Email">
                            </div>
                            <div class="mb-4">
                                <label for="example-password-input" class="col-form-label">Password</label>
                                <input name="txtPassword"
                                       class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-py-15 g-px-15"
                                       type="password" placeholder="Password">
                            </div>
                            <div class="mb-4">
                                <label for="example-date-of-birth-input" class="col-form-label">Date Of Birth</label>
                                <input name="txtDOB" class="form-control rounded-0 form-control-md" type="date"
                                       value="2011-08-19"
                                       id="example-date-input">
                            </div>
                            <div class="mb-4">
                                <label for="example-date-of-gender-input" class="col-form-label">Gender</label>
                                <div class="g-mb-15">
                                    <label class="form-check-inline u-check g-pl-25 ml-0 g-mr-25">
                                        <input name="txtGender" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                                               name="radInline1_1" type="radio" checked="">
                                        <div class="u-check-icon-radio-v4 g-absolute-centered--y g-left-0 g-width-18 g-height-18">
                                            <i class="g-absolute-centered d-block g-width-10 g-height-10 g-bg-primary--checked"></i>
                                        </div>
                                        Male
                                    </label>

                                    <label class="form-check-inline u-check g-pl-25 ml-0 g-mr-25">
                                        <input name="txtGender" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                                               name="radInline1_1" type="radio">
                                        <div class="u-check-icon-radio-v4 g-absolute-centered--y g-left-0 g-width-18 g-height-18">
                                            <i class="g-absolute-centered d-block g-width-10 g-height-10 g-bg-primary--checked"></i>
                                        </div>
                                        Female
                                    </label>

                                    <label class="form-check-inline u-check g-pl-25 ml-0 g-mr-25">
                                        <input name="txtGender" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                                               name="radInline1_1" type="radio">
                                        <div class="u-check-icon-radio-v4 g-absolute-centered--y g-left-0 g-width-18 g-height-18">
                                            <i class="g-absolute-centered d-block g-width-10 g-height-10 g-bg-primary--checked"></i>
                                        </div>
                                        Other
                                    </label>
                                </div>
                            </div>
                            <div class="mb-4">
                                <label name="txtAddress" for="example-address-input"
                                       class="col-form-label">Address</label>
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                       type="text" placeholder="Address">
                            </div>
                            <div class="mb-4">
                                <label for="example-occupation-input" class="col-form-label">Occupations</label>
                                <div class="g-mb-20">
                                    <label class="form-check-inline u-check g-pl-25">
                                        <input name="txtOccupation[1]" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                                               type="checkbox">
                                        <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        USA
                                    </label>

                                    <label class="form-check-inline u-check g-pl-25">
                                        <input name="txtOccupation[2]" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                                               type="checkbox">
                                        <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        Sweden
                                    </label>
                                </div>
                            </div>
                            <div class="mb-4">
                                <label for="example-topic-input" class="col-form-label">Topics</label>
                                <div class="g-mb-20">
                                    <label class="form-check-inline u-check g-pl-25">
                                        <input name="txtTopic[1]" class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                                               type="checkbox">
                                        <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        USA
                                    </label>

                                    <label class="form-check-inline u-check g-pl-25">
                                        <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                        <div name="txtTopic[2]"
                                             class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        Sweden
                                    </label>
                                </div>
                            </div>

                            <div class="text-center mb-5">
                                <button class="btn btn-block u-btn-primary rounded g-py-13" type="submit">Signup
                                </button>
                            </div>
                        </form>
                    @else
                        <header class="text-center mb-4">
                            <h2 class="h2 g-color-black g-font-weight-600">Sign up with a social network</h2>
                        </header>
                        <!-- Form -->
                        <form class="g-py-15">
                            <!-- Form Social Icons -->
                            <ul class="list-inline text-center mb-4">
                                <li class="list-inline-item g-mx-2">
                                    <a class="u-icon-v1 u-icon-size--sm u-icon-slide-up--hover g-color-white g-bg-facebook rounded-circle"
                                       href="redirect/facebook">
                                        <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-facebook"></i>
                                        <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-facebook"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item g-mx-2">
                                    <a class="u-icon-v1 u-icon-size--sm u-icon-slide-up--hover g-color-white g-bg-instagram rounded-circle"
                                       href="redirect/instagram">
                                        <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-instagram"></i>
                                        <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-instagram"></i>
                                    </a>
                                </li>
                                <li class="list-inline-item g-mx-2">
                                    <a class="u-icon-v1 u-icon-size--sm u-icon-slide-up--hover g-color-white g-bg-google-plus rounded-circle"
                                       href="redirect/google">
                                        <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-google-plus"></i>
                                        <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-google-plus"></i>
                                    </a>
                                </li>
                            </ul>
                            <!-- End Form Social Icons -->
                        </form>
                        <!-- End Form -->
                    @endif
                    <footer class="text-center">
                        <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Already have an account? <a
                                    class="g-font-weight-600" href="/">login</a>
                        </p>
                    </footer>
                </div>
            </div>
        </div>
    </section>
    <!-- End Signup -->
@endsection