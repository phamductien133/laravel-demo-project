<!-- JS Global Compulsory -->
<script src={{ asset('public/frontend/vendor/jquery/jquery.min.js')}}></script>
<script src={{ asset('public/frontend/vendor/jquery-migrate/jquery-migrate.min.js')}}></script>
<script src={{ asset('public/frontend/vendor/jquery.easing/js/jquery.easing.js')}}></script>
<script src={{ asset('public/frontend/vendor/popper.min.js')}}></script>
<script src={{ asset('public/frontend/vendor/bootstrap/bootstrap.min.js')}}></script>

<!-- JS Implementing Plugins -->
<script src={{ asset('public/frontend/vendor/hs-megamenu/src/hs.megamenu.js')}}></script>
<script src={{ asset('public/frontend/vendor/dzsparallaxer/dzsparallaxer.js')}}></script>
<script src={{ asset('public/frontend/vendor/dzsparallaxer/dzsscroller/scroller.js')}}></script>
<script src={{ asset('public/frontend/vendor/dzsparallaxer/advancedscroller/plugin.js')}}></script>
<script src={{ asset('public/frontend/vendor/chosen/chosen.jquery.js')}}></script>
<script src={{ asset('public/frontend/vendor/image-select/src/ImageSelect.jquery.js')}}></script>
<script src={{ asset('public/frontend/vendor/masonry/dist/masonry.pkgd.min.js')}}></script>
<script src={{ asset('public/frontend/vendor/slick-carousel/slick/slick.js')}}></script>

<!-- JS Unify -->
<script src={{ asset('public/frontend/js/hs.core.js')}}></script>
<script src={{ asset('public/frontend/js/components/hs.header.js')}}></script>
<script src={{ asset('public/frontend/js/helpers/hs.hamburgers.js')}}></script>
<script src={{ asset('public/frontend/js/components/hs.scroll-nav.js')}}></script>
<script src={{ asset('public/frontend/js/components/hs.go-to.js')}}></script>
<script src={{ asset('public/frontend/js/components/hs.sticky-block.js')}}></script>
<script src={{ asset('public/frontend/js/helpers/hs.height-calc.js')}}></script>
<script src={{ asset('public/frontend/js/components/hs.carousel.js')}}></script>

<!-- JS Custom -->
<script src={{ asset('public/frontend/js/custom.js')}}></script>

<!-- JS Plugins Init. -->
<script>
    $(document).on('ready', function () {
        // initialization of carousel
        $.HSCore.components.HSCarousel.init('.js-carousel');

        $('#carousel1').slick('setOption', 'responsive', [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3
            }
        }, {
            breakpoint: 992,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2
            }
        }, {
            breakpoint: 576,
            settings: {
                slidesToShow: 1
            }
        }, {
            breakpoint: 446,
            settings: {
                slidesToShow: 1
            }
        }], true);

        // Header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu plugin
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');

        $.HSCore.helpers.HSHeightCalc.init();
    });

    $(window).on('load', function () {
        // initialization of HSScrollNav
        $.HSCore.components.HSScrollNav.init($('#js-scroll-nav'), {
            duration: 700,
            easing: 'easeOutExpo',
            over: $('.u-secondary-navigation')
        });

        // initialization of masonry.js
        $('.masonry-grid').imagesLoaded().then(function () {
            $('.masonry-grid').masonry({
                // options
                columnWidth: '.masonry-grid-sizer',
                itemSelector: '.masonry-grid-item',
                percentPosition: true
            });
        });

        // initialization of sticky blocks
        $.HSCore.components.HSStickyBlock.init('.js-sticky-block');
    });
</script>