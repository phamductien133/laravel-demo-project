@if (Auth::check())
    <header id="js-header" class="u-header u-header--static">
        <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
            <nav class="js-mega-menu navbar navbar-expand-lg">
                <div class="container">
                    <!-- Navigation -->
                    <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg g-mr-40--lg"
                         id="navBar">
                    </div>
                    <div class="d-inline-block g-hidden-xs-down g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg">
                        <a class="btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15"
                           href="{{ route('getLogout') }}">Logout</a>
                    </div>
                </div>
            </nav>
        </div>
    </header>
@endif