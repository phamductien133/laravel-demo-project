@extends('auth/base/master')
@section('pageTitle', 'Hiip | Login')
@section('content')
    <section class="container g-py-150">
        <div class="row justify-content-center">
            <div class="col-sm-8 col-lg-6">
                <div class="g-brd-around g-brd-gray-light-v4 rounded g-py-40 g-px-30">
                    <header class="text-center mb-4">
                        <h2 class="h2 g-color-black g-font-weight-600">LLLogin</h2>
                    </header>
                    @if (count($errors) >0)
                        <ul>
                            @foreach($errors->all() as $error)
                                <li class="text-danger"> {{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    @if (session('status'))
                        <ul>
                            <li class="text-danger"> {{ session('status') }}</li>
                        </ul>
                    @endif
                <!-- Form -->
                    <form action="/login" method="post" class="g-py-15">
                        {{ csrf_field() }}
                        <div class="mb-4">
                            <div class="input-group g-brd-primary--focus">
                  <span class="input-group-addon g-width-45 g-brd-gray-light-v4 g-color-gray-dark-v5">
                      <i class="icon-finance-067 u-line-icon-pro"></i>
                    </span>
                                <input name="txtEmail" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-py-15 g-px-15"
                                       type="email" placeholder="johndoe@gmail.com">
                            </div>
                        </div>
                        <div class="g-mb-35">
                            <div class="input-group g-brd-primary--focus mb-4">
                  <span class="input-group-addon g-width-45 g-brd-gray-light-v4 g-color-gray-dark-v5">
                      <i class="icon-media-094 u-line-icon-pro"></i>
                    </span>
                                <input name="txtPassword" class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-py-15 g-px-15"
                                       type="password" placeholder="Password">
                            </div>
                        </div>

                        <div class="mb-5">
                            <button type="submit" class="btn btn-md btn-block u-btn-primary g-py-13">Login</button>
                        </div>

                        <div class="d-flex justify-content-center text-center g-mb-30">
                            <div class="d-inline-block align-self-center g-width-50 g-height-1 g-bg-gray-light-v1"></div>
                            <span class="align-self-center g-color-gray-dark-v3 mx-4">OR</span>
                            <div class="d-inline-block align-self-center g-width-50 g-height-1 g-bg-gray-light-v1"></div>
                        </div>

                        <!-- Form Social Icons -->
                        <ul class="list-inline text-center mb-4">
                            <li class="list-inline-item g-mx-2">
                                <a class="u-icon-v1 u-icon-size--sm u-icon-slide-up--hover g-color-white g-bg-facebook rounded-circle"
                                   href="redirect/facebook">
                                    <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-facebook"></i>
                                    <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-facebook"></i>
                                </a>
                            </li>
                            <li class="list-inline-item g-mx-2">
                                <a class="u-icon-v1 u-icon-size--sm u-icon-slide-up--hover g-color-white g-bg-instagram rounded-circle"
                                   href="redirect/instagram">
                                    <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-instagram"></i>
                                    <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-instagram"></i>
                                </a>
                            </li>
                            <li class="list-inline-item g-mx-2">
                                <a class="u-icon-v1 u-icon-size--sm u-icon-slide-up--hover g-color-white g-bg-google-plus rounded-circle"
                                   href="redirect/google">
                                    <i class="g-font-size-default g-line-height-1 u-icon__elem-regular fa fa-google-plus"></i>
                                    <i class="g-font-size-default g-line-height-0_8 u-icon__elem-hover fa fa-google-plus"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- End Form Social Icons -->
                    </form>
                    <!-- End Form -->

                    <footer class="text-center">
                        <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Don't have an account? <a
                                    class="g-font-weight-600" href="{{ route('getSocialRegister') }}">register</a>
                        </p>
                    </footer>
                </div>
            </div>
        </div>
    </section>
@endsection