<!-- Favicon -->
<link rel="shortcut icon" href={{ asset('public/favicon.ico')}}>
<!-- Google Fonts -->
<link rel="stylesheet"
      href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C500%2C600%2C700%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">
<!-- CSS Global Compulsory -->
<link rel="stylesheet" href={{ asset('public/frontend/vendor/bootstrap/bootstrap.min.css')}}>
<!-- CSS Global Icons -->
<link rel="stylesheet" href={{ asset('public/frontend/vendor/icon-awesome/css/font-awesome.min.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/icon-line/css/simple-line-icons.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/icon-etlinefont/style.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/icon-line-pro/style.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/icon-hs/style.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/dzsparallaxer/dzsparallaxer.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/dzsparallaxer/dzsscroller/scroller.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/dzsparallaxer/advancedscroller/plugin.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/slick-carousel/slick/slick.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/animate.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/hs-megamenu/src/hs.megamenu.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/vendor/hamburgers/hamburgers.min.css')}}>
<!-- CSS Unify -->
<link rel="stylesheet" href={{ asset('public/frontend/css/unify-core.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/css/unify-components.css')}}>
<link rel="stylesheet" href={{ asset('public/frontend/css/unify-globals.css')}}>
<!-- CSS Customization -->
<link rel="stylesheet" href={{ asset('public/frontend/css/custom.css')}}>

