<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <!-- Title -->
    <title>Hiip - @yield('pageTitle')</title>
    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    @include('auth.base.head')
</head>
<body>
<main>
    <!-- Header -->
    @include('auth.base.header')
    <!-- End Header -->

    <!-- Content -->
    @yield('content')
    <!-- End Content -->

    <!-- Footer -->
    @include('auth.base.footer')
    <!-- End Footer -->

    <a class="js-go-to u-go-to-v1 animated js-animation-was-fired zoomIn" href="#!" data-type="fixed" data-position="{
     &quot;bottom&quot;: 15,
     &quot;right&quot;: 15
   }" data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn" style="display: inline-block; position: fixed; bottom: 15px; right: 15px;">
        <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
</main>
<div class="u-outer-spaces-helper"></div>
<!-- Scripts -->
@include('auth.base.scripts')
<!-- End Scripts -->
</body>
</html>
