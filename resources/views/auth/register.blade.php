@extends('auth/base/master')
@section('pageTitle', $data['title'])
@section('content')
    <!-- Signup -->
    <section class="container g-py-100">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-md-9 col-lg-6">
                <div class="g-brd-around g-brd-gray-light-v4 rounded g-py-40 g-px-30">
                    <header class="text-center mb-4">
                        <h2 class="h2 g-color-black g-font-weight-600">Sign up</h2>
                    </header>
                    @if (count($errors) >0)
                        <ul>
                            @foreach($errors->all() as $error)
                                <li class="text-danger"> {{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    @if ($status)
                        <ul>
                            <li class="text-danger"> {{ $status }}</li>
                        </ul>
                    @endif
                    <form class="g-py-15" method="POST" action="{{ route('postRegister') }}">
                        @csrf
                        <div class="mb-4">
                            <label for="full-name" class="col-form-label">Full Name</label>
                            <input name="name"
                                   id="name"
                                   class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                   type="text" placeholder="Full Name"
                                   value="{{$data['name']}}" required autofocus>
                        </div>
                        <div class="d-none">
                            <input name="check" id="check" value="{{$check}}"
                                   class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15">
                        </div>
                        <div class="d-none">
                            <input name="provider_id" id="provider_id" value="{{$data['provider_id']}}"
                                   class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15">
                        </div>
                        <div class="mb-4 @if( $check) d-none @endif">
                            <label for="email" class="col-form-label">Email</label>
                            <input name="email"
                                   id="email"
                                   class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                   placeholder="Email"
                                   type="email"
                                   value="{{$data['email']}}" @if( !$check) required autofocus @endif>
                        </div>
                        <div class="mb-4">
                            <label for="phone-number" class="col-form-label">Phone Number</label>
                            <input name="phoneNumber"
                                   id="phoneNumber"
                                   class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                   type="text" placeholder="Phone Number"
                                   value="{{$data['phone_number']}}" required autofocus>
                        </div>
                        <div class="mb-4">
                            <label for="password" class="col-form-label">Password</label>
                            <input id="password"
                                   name="password"
                                   class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-py-15 g-px-15"
                                   type="password" placeholder="Password"
                                   required>
                        </div>
                        <div class="mb-4">
                            <label for="password_confirm" class="col-form-label">Confirm Password</label>
                            <input name="password_confirm"
                                   id="password_confirm"
                                   class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-py-15 g-px-15"
                                   type="password" placeholder="Confirm Password"
                                   required>
                        </div>
                        <div class="mb-4">
                            <label for="date-of-birth" class="col-form-label">Date Of Birth</label>
                            <input name="dob"
                                   id="dob"
                                   class="form-control rounded-0 form-control-md"
                                   type="date"
                                   value="{{$data['dob']}}" required autofocus>
                        </div>
                        <div class="mb-4">
                            <label for="gender" class="col-form-label">Gender</label>
                            <div class="g-mb-15">
                                <select id="gender" name="gender" class="custom-select mb-3">
                                    <option selected="">Choose...</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                            </div>
                        </div>
                        <div class="mb-4">
                            <label for="address"
                                   class="col-form-label">Address</label>
                            <input name="address"
                                   id="address"
                                   class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                   type="text" placeholder="Address"
                                   value="{{$data['address']}}" required autofocus>
                        </div>
                        <div class="mb-4">
                            <label for="example-occupation-input" class="col-form-label">Occupations</label>
                            <div class="g-mb-20">
                                @foreach($occupations as $occupation)
                                    <label class="form-check-inline u-check g-pl-25">
                                        <input name="occupation[{{$occupation->id}}]"
                                               class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                                               id="occupation[{{$occupation->id}}]" type="checkbox">
                                        <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        {{$occupation->name}}
                                    </label>
                                @endforeach
                            </div>
                        </div>
                        <div class="mb-4">
                            <label for="example-topic-input" class="col-form-label">Topics</label>
                            <div class="g-mb-20">
                                @foreach($topics as $topic)
                                    <label class="form-check-inline u-check g-pl-25">
                                        <input name="occupation[{{$topic->id}}}]"
                                               class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"
                                               id="occupation[{{$topic->id}}]" type="checkbox">
                                        <div class="u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0">
                                            <i class="fa" data-check-icon=""></i>
                                        </div>
                                        {{$topic->name}}
                                    </label>
                                @endforeach
                            </div>
                        </div>

                        <div class="text-center mb-5">
                            <button class="btn btn-block u-btn-primary rounded g-py-13" type="submit">Signup
                            </button>
                        </div>
                    </form>
                    <!-- End Form -->
                    <footer class="text-center">
                        <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">Already have an account? <a
                                    class="g-font-weight-600" href="/login">login</a>
                        </p>
                    </footer>
                </div>
            </div>
        </div>
    </section>
@endsection
